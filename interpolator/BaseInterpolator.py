"""
sippy.interpolator.BaseInterpolator:
Base python class and methods for creating interpolators between
stellar parameters.
Author: Lyra Cao
3/9/2018
"""

import numpy as np
from astropy import units as u
import re
import itertools as it
import random
import scipy.interpolate

class BaseInterpolator(object):
    """Base class for interpolators."""
    def __init__(self):
        raise NotImplementedError
    def _evaluate(self, values):
        raise NotImplementedError
    def evaluate(self, values, *args, **kwargs):
        """Evaluate and interpolate a given array or value."""
        if isinstance(values, u.Quantity):
            # If so, set it.
            values = values.to(self._default_input_unit).value
        ### Polymorphism:
        ### Check if additional arguments are given. If so, we need
        ### to convert each input value systematically.
        if len(args) > 0:
            for index in range(len(args)):
                # Check to see if the values passed have a dimensional component
                if isinstance(args[index], u.Quantity):
                    #Set the dimensional component for each value.
                    args[index] =\
                    args[index].to(self._default_input_unit[index+1]).value
            if len(kwargs.keys()) > 0:
                # Check whether kwargs is used as a key. (TODO: replace)
                if "kwargs" in kwargs.keys():
                    output = self._evaluate(values, *args)
                else:
                    output = self._evaluate(values, *args, **kwargs)
            else:
                output = self._evaluate(values, *args)
        else:
            if len(kwargs.keys()) > 0:
                # Check whether kwargs is used as a key. (TODO: replace)
                if "kwargs" in kwargs.keys():
                    output = self._evaluate(values)
                else:
                    output = self._evaluate(values, **kwargs)
            else:
                output = self._evaluate(values)
        # Convert from the default output unit to the specified output unit
        output *= (self.output_unit/self._default_output_unit).\
        in_units(u.dimensionless_unscaled)
        ## Another possibility, though it will not error if there is a
        ## problem with units:
        #output *= (self.output_unit/self._default_output_unit).\
        #decompose().scale
        return output
    def __call__(self, values, *args, **kwargs):
        """Allows calling the class-level object with evaluate's arguments.
        Returns the values with proper units attached.
        To avoid returning the units as well, use evaluate()."""
        return self.evaluate(values, *args, **kwargs) * self.output_unit
    def __add__(self, other):
        """Allows adding interpolators and numbers.
        """
        return AddInterpolator(self, other)
    def __sub__(self, other):
        """Allows subtracting interpolators and numbers.
        """
        return SubInterpolator(self, other)
    def __mul__(self, other):
        """Allows multiplying interpolators and numbers.
        """
        return MulInterpolator(self, other)
    def __div__(self, other):
        """Allows dividing interpolators and numbers.
        """
        return DivInterpolator(self, other)

class AddInterpolator(BaseInterpolator):
    def __init__(self, interp1, interp2):
        self.interp1 = interp1
        self.interp2 = interp2
        # Assume units are the same --- it is addition, after all.
        self.output_unit = interp1.output_unit
        self._default_input_unit = interp1._default_input_unit
        self._default_output_unit = interp1._default_output_unit
    def evaluate(self, values, *args, **kwargs):
        #Check if interp2 is a BaseInterpolator:
        if isinstance(self.interp2, BaseInterpolator):
            return_val =\
            self.interp1.evaluate(values, *args, **kwargs) +\
            self.interp2.evaluate(values, *args, **kwargs)
        else:
            return_val =\
            self.interp1.evaluate(values, *args, **kwargs) +\
            self.interp2
        return return_val

class SubInterpolator(BaseInterpolator):
    def __init__(self, interp1, interp2):
        self.interp1 = interp1
        self.interp2 = interp2
        # Assume units are the same --- it is subtraction, after all.
        self.output_unit = interp1.output_unit
        self._default_input_unit = interp1._default_input_unit
        self._default_output_unit = interp1._default_output_unit
    def evaluate(self, values, *args, **kwargs):
        #Check if interp2 is a BaseInterpolator:
        if isinstance(self.interp2, BaseInterpolator):
            return_val =\
            self.interp1.evaluate(values, *args, **kwargs) -\
            self.interp2.evaluate(values, *args, **kwargs)
        else:
            return_val =\
            self.interp1.evaluate(values, *args, **kwargs) -\
            self.interp2
        return return_val

class MulInterpolator(BaseInterpolator):
    def __init__(self, interp1, interp2):
        self.interp1 = interp1
        self.interp2 = interp2
        self._default_input_unit = interp1._default_input_unit
        # Units are multiplied. But we have to check whether interp2 has an
        # output unit.
        if hasattr(interp2, "output_unit"):
            self.output_unit = interp1.output_unit*interp2.output_unit
            self._default_output_unit =\
            interp1._default_output_unit*\
            interp2._default_output_unit
        # If it doesn't, check if it has a unit:
        elif hasattr(interp2, "unit"):
            self.output_unit = interp1.output_unit*interp2.unit
            self._default_output_unit =\
            interp1._default_output_unit*\
            interp2.unit
        # Otherwise, just assume it's a scale factor:
        else:
            self.output_unit = interp1.output_unit
            self._default_output_unit =\
            interp1._default_output_unit
    def evaluate(self, values, *args, **kwargs):
        #Check if interp2 is a BaseInterpolator:
        if isinstance(self.interp2, BaseInterpolator):
            return_val =\
            self.interp1.evaluate(values, *args, **kwargs) *\
            self.interp2.evaluate(values, *args, **kwargs)
        else:
            if hasattr(self.interp2, "unit"):
                # It has a unit, so we have to get only its value here.
                interp2 = self.interp2.value
            else:
                # It doesn't have a unit. Scale factor?
                interp2 = self.interp2
            return_val =\
            self.interp1.evaluate(values, *args, **kwargs) *\
            interp2
        return return_val

class DivInterpolator(BaseInterpolator):
    def __init__(self, interp1, interp2):
        self.interp1 = interp1
        self.interp2 = interp2
        self._default_input_unit = interp1._default_input_unit
        # Units are multiplied. But we have to check whether interp2 has an
        # output unit.
        if hasattr(interp2, "output_unit"):
            self.output_unit = interp1.output_unit/interp2.output_unit
            self._default_output_unit =\
            interp1._default_output_unit/\
            interp2._default_output_unit
        # If it doesn't, check if it has a unit:
        elif hasattr(interp2, "unit"):
            self.output_unit = interp1.output_unit/interp2.unit
            self._default_output_unit =\
            interp1._default_output_unit/\
            interp2.unit
        # Otherwise, just assume it's a scale factor:
        else:
            self.output_unit = interp1.output_unit
            self._default_output_unit =\
            interp1._default_output_unit
    def evaluate(self, values, *args, **kwargs):
        #Check if interp2 is a BaseInterpolator:
        if isinstance(self.interp2, BaseInterpolator):
            return_val =\
            self.interp1.evaluate(values, *args, **kwargs) /\
            self.interp2.evaluate(values, *args, **kwargs)
        else:
            if hasattr(self.interp2, "unit"):
                # It has a unit, so we have to get only its value here.
                interp2 = self.interp2.value
            else:
                # It doesn't have a unit. Scale factor?
                interp2 = self.interp2
            return_val =\
            self.interp1.evaluate(values, *args, **kwargs) /\
            interp2
        return return_val

class NegativeInterpolator(BaseInterpolator):
    def __init__(self, interp):
        self.interp = interp
        # Units are the same.
        self.output_unit = interp.output_unit
        self._default_input_unit = interp._default_input_unit
        self._default_output_unit = interp._default_output_unit
    def evaluate(self, values, *args, **kwargs):
        # The return value is simply the negative evaluation.
        return_val =\
        -self.interp.evaluate(values, *args, **kwargs)
        return return_val

class SpTBaseInterpolator(BaseInterpolator):
    """Base class for interpolators, with additional SpT helper functions.

    Note: evaluate___SpT_input currently
    does not support polymorphism like the BaseInterpolator's evaluate.

    There hasn't really been a need for this in the interpolation context,
    but if a function ever needs to pass in an argument list that has a
    SpT in it, then evaluate___SpT_input needs to be rewritten."""
    decimals = 3 # Significant figures to round to in output string SpT's
    spectral_types=["O", "B", "A", "F", "G", "K", "M", "L", "T", "Y"]
    zero_point=0
    @classmethod
    def set_spectral_types(self, spectral_types=["O", "B", "A", "F", "G",
                                                 "K", "M", "L", "T", "Y"]):
        """Sets the ordered list of spectral types that are used,
        starting with O.

        The first spectral type used corresponds to the zero_point
        (nominally "O")"""
        self.spectral_types = spectral_types
    @classmethod
    def set_SpT_zero_point(self, zero_point=0):
        """Sets the SpT zero_point consistently for the routines:
        SpT_to_num and num_to_SpT.

        By default, the "O" class corresponds to 0, ie: O7V corresponding to 7,
        but it can be changed to "O" corresponding to 10, and O7V corresponding
        to 17, by setting the zero_point to 1.

        Note: if set_spectral_types is invoked with a list with a different
        first item, that is what this zero point will refer to instead of "O".
        """
        self.zero_point = zero_point
    @classmethod
    def SpT_to_num(self, SpT):
        """Helper function for conversion of a single SpT to a single
        numerical value. Ignores luminosity class.

        Input: string SpT
        Output: float SpT

        Returns np.NaN in other cases!"""
        if isinstance(SpT, str):
            # Strip SpT of whitespace:
            SpT = SpT.strip()
            ### Use regex to match the SpT literal:
            SpT_class = re.match("[A-Za-z]+", SpT).group(0)
            # Find the index corresponding to that SpT in the spectral types
            # list, set in set_spectral_types.
            SpT_index = self.spectral_types.index(SpT_class)
            # Find the spectral type with the correct zero point set by
            # set_SpT_zero_point.
            SpT_index = SpT_index + self.zero_point

            ### Use regex to match the numerical literal:
            num_SpT_subclass = float(re.search("[0-9.]+", SpT).group(0))

            ### The end SpT numerical value is:
            num = SpT_index*10. + num_SpT_subclass
            return num
        else: return np.NaN
    def num_to_SpT(self, num):
        """Helper function for conversion of numerical values to SpT. Does not
        return luminosity class.

        Input: float SpT
        Output: string SpT

        Returns "nan" if a string is passed, or the SpT is a nonsensical value.
        """
        if isinstance(num, str):
            return "nan"
        else:
            if np.isfinite(num):
                ### Find the SpT class:
                SpT_index = int(num/10)
                # Correct with the zero point set by set_SpT_zero_point
                SpT_index = SpT_index - self.zero_point
                # Find the SpT class:
                SpT_class = self.spectral_types[SpT_index]

                ### Add the additional part:
                num_SpT_subclass = np.around(num - int(num/10)*10,
                                             decimals = self.decimals)
                SpT_subclass = str(num_SpT_subclass)

                ### Create the SpT:
                SpT = SpT_class + SpT_subclass

                return SpT
            else: return "nan"
    @classmethod
    def get_num_SpTs(self, SpTs):
        """Gets a numpy array of numerical SpTs given any supported input:

        * String SpT (i.e., A7.5V)
        * Astropy unit (i.e., 27.5*u.dimensionless_unscaled)
        * Float (i.e., 27.5)
        """
        ### Currently have to check if input is lists/np arrays or just a value:
        if hasattr(SpTs, "__len__") and (not isinstance(SpTs, str)):
            # Yes, this is a list/np array.
            # Find the total number of SpT's passed to us.
            return np.array([self.get_num_SpTs(SpT) for SpT in SpTs])
        else:
            # No, this is a single item, like a string or float.
            SpT = SpTs
            if isinstance(SpT, u.Quantity):
                num_SpT =\
                SpT.to(self._default_input_unit).value
            # If this is a float, just copy it over:
            elif isinstance(SpT, float) or isinstance(SpT, int):
                num_SpT = SpT
            # Otherwise, consider it a string to be converted:
            else:
                num_SpT = self.SpT_to_num(SpT)
            return num_SpT
    def get_SpTs(self, num_SpTs):
        """Takes a list of numerical SpT's and returns a list of string SpTs"""
        # Check if the numerical SpTs passed are a list/np array or a
        # scalar float.
        # Coerce to a numpy array.
        if hasattr(num_SpTs, "__len__"):
            # Yes, this is a list of values:
            return [self.get_SpTs(num_SpT) for num_SpT in num_SpTs]
        else:
            # No, this is a single value.
            num_SpT = num_SpTs
            SpT = self.num_to_SpT(num_SpT)
            return SpT
    def evaluate___SpT_input(self,  values, *args, **kwargs):
        """The evaluate method, modified for SpT inputs."""
        # Convert from SpT input:
        values = self.get_num_SpTs(values)
        # Evaluate interpolator with these numerical SpTs:
        output = self._evaluate(values)
        # Convert from the default output unit to the specified output unit
        # If the output unit is the string "SpT", then we convert back:
        if self.output_unit == "SpT":
            # Convert to a string SpT:
            output = self.get_SpTs(output)
        else:
            output *= (self.output_unit/self._default_output_unit).\
            in_units(u.dimensionless_unscaled)
        return output
    def evaluate___SpT_output(self, values, *args, **kwargs):
        """The evaluate method, modified for SpT outputs."""
        if isinstance(values, u.Quantity):
            # If so, set it.
            values = values.to(self._default_input_unit).value
        ### Polymorphism:
        ### Check if additional arguments are given. If so, we need
        ### to convert each input value systematically.
        if len(args) > 0:
            for index in range(len(args)):
                # Check to see if the values passed have a dimensional component
                if isinstance(args[index], u.Quantity):
                    #Set the dimensional component for each value.
                    args[index] =\
                    args[index].to(self._default_input_unit[index+1]).value
            output = self._evaluate(values, *args)
        else:
            output = self._evaluate(values)
        # Convert from the default output unit to the specified output unit
        # If the output unit is the string "SpT", then we convert back:
        if self.output_unit == "SpT":
            # Convert to a string SpT:
            output = self.get_SpTs(output)
        else:
            output *= (self.output_unit/self._default_output_unit).\
            in_units(u.dimensionless_unscaled)
        return output

class MergedFallbackInstantiatedInterpolator(BaseInterpolator):
    """Provides "fallback interpolation" for an ordered list of similar
    instantiated interpolators that have the same input and output types.

    Instantiated means that the input and output of the interpolators has
    already been chosen.

    Fallback interpolation is defined as using the first interpolator in the
    list, and then replacing all NaN values with the second interpolator,
    and so on."""
    def __init__(self,interpolators):
        self.interpolators = interpolators
        # Assume the first interpolator is representative of all the
        # interpolators. Set the default inputs and outputs to that
        # interpolator.
        self._default_input_unit = self.interpolators[0]._default_input_unit
        self._default_output_unit = self.interpolators[0]._default_output_unit
    def add_interpolator_last(self, interpolator):
        """Adds an interpolator to the end of the ordered list
        of interpolators"""
        self.interpolators = self.interpolators + [interpolator]
    def add_interpolator_first(self, interpolator):
        """Adds an interpolator to the start of the ordered list
        of interpolators"""
        self.interpolators = [interpolator] + self.interpolators
    def evaluate(self, values, *args, **kwargs):
        """Iterates over all child interpolators using the fallback scheme"""
        # Save all the argument calls for child interpolators.
        saved_args = locals().copy()
        # Remove "self" argument from the saved arguments so we can
        # Use it to evaluate our interpolators later in their own context.
        saved_args.pop("self", None)
        # Make sure the input array for values is a numpy array:
        values = np.array(values)
        ### Iterate over all the interpolators, replacing NaN values with
        ### data from the next interpolator.
        N = len(self.interpolators) # Number of interpolators
        # Get the evaluation of the first interpolator:
        output = self.interpolators[0].evaluate(**saved_args)
        # Iterate over all the rest
        for i in range(1, N):
            # Find the NaN values of the output
            nan_locations = np.isnan(output)
            # Amend the arguments to only use the
            # values corresponding to the NaN's of the previous iterator:
            saved_args["values"] = values[nan_locations]
            # Get the evaluation of the interpolator
            temp_output = self.interpolators[i].evaluate(**saved_args)
            # Amend output with this additional temp_output:
            output[nan_locations] = temp_output
        return output
    def __call__(self, values, *args, **kwargs):
        """Allows calling the class-level object with evaluate's arguments.
        Returns the values with proper units attached.
        To avoid returning the units as well, use evaluate().

        Since MergedFallbackInterpolator is a list of interpolators with similar
        behavior, we use the output unit of the first interpolator in the chain.
        """
        return self.evaluate(values, *args, **kwargs) *\
        self.interpolators[0].output_unit

class MergedFallbackInterpolator(BaseInterpolator):
    """Provides "fallback interpolation" for an ordered list of similar
    uninstantiated interpolators that have the same input and output types.

    The method "on" constructs an interpolator on a specified input and output.

    Fallback interpolation is defined as using the first interpolator in the
    list, and then replacing all NaN values with the second interpolator,
    and so on.

    If uninstantiated color interpolators are added, available_colors,
    as well as available_input_colors, and available_output_colors,
    are defined."""
    def __init__(self,interpolators):
        self.interpolators = interpolators
        # Since the interpolators are not instantiated, the units are
        # ambiguous.
        # After using the key "on", input and output units can be defined.
        self.available_input_colors = []
        self.available_output_colors = []
        self.refresh()
    def add_interpolator_last(self, interpolator):
        """Adds an interpolator to the end of the ordered list
        of interpolators"""
        self.interpolators = self.interpolators + [interpolator]
        self.refresh()
    def add_interpolator_first(self, interpolator):
        """Adds an interpolator to the start of the ordered list
        of interpolators"""
        self.interpolators = [interpolator] + self.interpolators
        self.refresh()
    def refresh(self):
        ### Set the available colors for automated querying of this method.
        #Iterate over each interpolator:
        for interpolator in self.interpolators:
            if hasattr(interpolator, "available_colors"):
                for available_color in interpolator.available_colors:
                    if available_color not in self.available_input_colors:
                        # If there is a color that the interpolator supports
                        # that is not recorded in input, add it:
                        self.available_input_colors.append(available_color)
                    if available_color not in self.available_output_colors:
                        # If there is a color that the interpolator supports
                        # that is not recorded in output, add it:
                        self.available_output_colors.append(available_color)
            if hasattr(interpolator, "available_input_colors"):
                for available_color in interpolator.available_input_colors:
                    if available_color not in self.available_input_colors:
                        # If there is a color that the interpolator supports
                        # that is not recorded in input, add it:
                        self.available_input_colors.append(available_color)
            if hasattr(interpolator, "available_output_colors"):
                for available_color in interpolator.available_output_colors:
                    if available_color not in self.available_output_colors:
                        # If there is a color that the interpolator supports
                        # that is not recorded in output, add it:
                        self.available_output_colors.append(available_color)
    def on(self, input, output, **kwargs):
        interpolator_list = []
        for interpolator in self.interpolators:
            # Instantiate each interpolator, and where an interpolator has
            # the behavior, "on", use that to instantiate it.
            if hasattr(interpolator, "on"):
                instantiated_interpolator =\
                interpolator.on(input, output, **kwargs)
            else:
                instantiated_interpolator =\
                interpolator(input, output, **kwargs)
            # Append the newly instantiated interpolator to the list
            interpolator_list.append(instantiated_interpolator)
        #Create a new MergedFallbackInstantiatedInterpolator and give it
        #these instantiated interpolators.
        return MergedFallbackInstantiatedInterpolator(interpolator_list)

class GeneralizedSpTInterpolator():
    """Takes a color interpolator at a specific SpT and creates an interpolator
    that takes a wavelength and returns a color with a consistent second band,
    such as U-V, B-V, Ic-V, Ks-V ...

    Which can be turned into a spectrum by adding a zero-point photometry."""
    def __init__(self, SpT, ColorInterpolators = []):
        pass

class LinkedInstantiatedInterpolator(BaseInterpolator):
    """A simple interpolator class that runs through each instantiated
    interpolator and feeds the output of the preceding one to the current one
    in sequence."""
    def _evaluate(self, values, *args, **kwargs):
        output = values
        # Iterating over each interpolator, feed the output of one to the next
        # one in the list.
        for interpolator in self.interpolators:
            output = interpolator.evaluate(output, *args, **kwargs)
        return output
    def __init__(self, interpolators, output_unit = None):
        self.interpolators = interpolators
        # Set default inputs and outputs.
        self._default_input_unit = self.interpolators[0]._default_input_unit
        self._default_output_unit = self.interpolators[-1]._default_output_unit
        if output_unit is None:
            self.output_unit = self._default_output_unit
        else:
            self.output_unit = output_unit

class LinkedInterpolator(BaseInterpolator):
    """Returns an interpolator upon instantiation that links interpolators,
    by taking the output of the first interpolator as the input for the second
    interpolator, and so on.

    Must send these parameters to LinkedInterpolator.set().

    Interpolators is a list of uninstantiated interpolators.
    Links is a list of "link" parameters, which denotes the name of the column
    or input/output value that is shared between the interpolators, in sequence.
    There must be 1 less link than interpolators.

    For example, a value for Links of ["A", "B", "C"] would give the following:
    Interpolator 1: Input -> A
    Interpolator 2: A -> B
    Interpolator 3: B -> C
    Interpolator 4: C -> Output

    Where Input and Output is determined at runtime.

    arg_list is a list of dictionaries which correspond to the **kwargs to
    initialize each interpolator, in sequence.

    Usage:
    BTSettl_with_SpT =
    LinkedInterpolator([PecautMamajekYoungInterpolator,
                        BTSettlSDSSInterpolator],
                        ["Teff"],
                        arg_list = [{"interpolation_kind": "linear"},
                                    {"interpolation_kind": "linear",
                                     "logg": 4.4}])

    BTSettl_with_SpT.on("SpT", "g-z")
    BTSettl_with_SpT("B0V")

    Note: since this code takes uninstantiated interpolators,
    and Python doesn't support multiple constructors, it must be instantiated
    with ``on''.

    Under the hood, this evaluates:
    PecautMamajekYoungInterpolator("B0V") to get a Teff,
    and BTSettlSDSSInterpolator with that Teff.
    """
    def on(self, input, output, interpolation_kind = "linear",
                 output_unit = None):
        """Creates and returns a LinkedInstantiatedInterpolator on the
        specified input and output.

        If interpolation_kind is already specified in arg_list,
        that is the value that is used."""
        interpolators = []
        # The full links object has `input` as its first element
        # and `output` as its last element.
        full_links = [input] + self.links + [output]
        # Create input and output argument pairs:
        for index in range(len(full_links) - 1):
            argument_pairs = (full_links[index], full_links[index+1])
            #Which is now a list full of tuples from
            #Input to X, X to Y, Y to Output. (To arbitrary length.)
            if hasattr(self.interpolators[index], "on"):
                interpolators.append(self.interpolators[index]\
                                     .on(*argument_pairs))
            else:
                interpolators.append(self.interpolators[index]\
                                     (*argument_pairs))

        return LinkedInstantiatedInterpolator(interpolators)
    def __init__(self, interpolators, links, arg_list = None):
        """Instantiate a linked interpolator."""
        #assert isinstance(links, str),\
        #"The parameter for the links must be a list/tuple. "+\
        #"(It can be a list with one element.)"
        if isinstance(links, str):
            links = [links]
        assert len(links) == len(interpolators) - 1, "Wrong number of links"\
        +" for the number of interpolators passed."
        if arg_list is not None:
            assert len(arg_list) == len(interpolators), "Wrong number of"\
            +" argument dictionaries for the number of interpolators passed."
        else:
            # No keyword args to pass, so the arg_list is just an empty
            # dictionary
            arg_list = [dict() for x in range(len(interpolators))]
        self.interpolators = interpolators
        self.links = links
        self.arg_list = arg_list

        # If the first interpolator is a ColorInterpolator, then set
        # available_colors:
        if hasattr(self.interpolators[0], "available_colors"):
            self.available_input_colors =\
             self.interpolators[0].available_colors

        # If the last interpolator is a ColorInterpolator, then set
        # available_colors:
        if hasattr(self.interpolators[-1], "available_colors"):
            self.available_output_colors =\
             self.interpolators[-1].available_colors

        # If the first interpolator is a subclass that extends ColorInterpolator
        # set available_colors:
        if hasattr(self.interpolators[0], "available_input_colors"):
            self.available_input_colors =\
            self.interpolators[0].available_input_colors

        # If the last interpolator is a subclass that extends ColorInterpolator
        # set available_colors:
        if hasattr(self.interpolators[-1], "available_output_colors"):
            self.available_output_colors =\
            self.interpolators[-1].available_output_colors

class ExtendColorInterpolator(BaseInterpolator):
    """Returns a ColorInterpolator that routes colors to its child
    ColorInterpolators.

    Accepts an ordered list of ColorInterpolators upon initialization. When
    ColorInterpolators have repeats of available colors, only the first instance
    of the color will be used.

    Only novel colors will be added. For instance, if U-B and B-V are already
    defined, a new addition of a U-V color will be ignored, because U-V can be
    obtained with the existing colors.

    When initializing "on", only one of input or output can be a color.
    If both are colors, a better class to use might be LinkedInterpolator.

    Note: since this code takes uninstantiated interpolators,
    and Python doesn't support multiple constructors, it must be instantiated
    with ``on''."""
    def __init__(self, ColorInterpolators):
        # Polymorphism: even if a single ColorInterpolator is passed, this
        # will work.
        if not hasattr(ColorInterpolators, "__len__"):
            ColorInterpolators = [ColorInterpolators]
        self.interpolators = ColorInterpolators
        self.refresh()
    def refresh(self):
        """Refreshes the ColorInterpolator map."""
        self.route_input = {} #Routes for interpolators that input colors.
        self.route_output = {} #Routes for interpolators that output colors.
        # Iterate over each color in each ColorInterpolator,
        # Starting with the first ColorInterpolator's colors.
        # With these colors, create a routing table that points to
        # Each ColorInterpolator.
        for ColorInterpolator in self.interpolators:
            if hasattr(ColorInterpolator, "available_colors"):
                for available_color in ColorInterpolator.available_colors:
                    # Check whether the color is a duplicate (or can be obtained
                    # by existing colors).
                    # First create a list of tuples corresponding to each color:
                    route_input_keys_tuple =\
                    [x.split("-") for x in self.route_input.keys()]
                    route_output_keys_tuple =\
                    [x.split("-") for x in self.route_output.keys()]
                    # Create a graph corresponding to these tuples.
                    graph_input =\
                    self.color_list_to_graphs(route_input_keys_tuple)
                    graph_output =\
                    self.color_list_to_graphs(route_output_keys_tuple)
                    # If there is no path between the color in question, add it
                    if self.find_shortest_path(graph_input,
                                               available_color.split("-")[0],
                                               available_color.split("-")[1])\
                    is None:
                        # If the color is not in the route, we add it.
                        self.route_input[available_color] = ColorInterpolator
                    if self.find_shortest_path(graph_output,
                                               available_color.split("-")[0],
                                               available_color.split("-")[1])\
                    is None:
                        self.route_output[available_color] = ColorInterpolator
            if hasattr(ColorInterpolator, "available_input_colors"):
                for available_color in ColorInterpolator.available_input_colors:
                    # Check whether the color is a duplicate (or can be obtained
                    # by existing colors).
                    # First create a list of tuples corresponding to each color:
                    route_input_keys_tuple =\
                    [x.split("-") for x in self.route_input.keys()]
                    # Create a graph corresponding to these tuples.
                    graph_input =\
                    self.color_list_to_graphs(route_input_keys_tuple)
                    # If there is no path between the color in question, add it
                    if self.find_shortest_path(graph_input,
                                               available_color.split("-")[0],
                                               available_color.split("-")[1])\
                    is None:
                        # If the color is not in the route, we add it.
                        self.route_input[available_color] = ColorInterpolator
            if hasattr(ColorInterpolator, "available_output_colors"):
                for available_color in ColorInterpolator.available_output_colors:
                    # Check whether the color is a duplicate (or can be obtained
                    # by existing colors).
                    # First create a list of tuples corresponding to each color:
                    route_output_keys_tuple =\
                    [x.split("-") for x in self.route_output.keys()]
                    # Create a graph corresponding to these tuples.
                    graph_output =\
                    self.color_list_to_graphs(route_output_keys_tuple)
                    # If there is no path between the color in question, add it
                    if self.find_shortest_path(graph_output,
                                               available_color.split("-")[0],
                                               available_color.split("-")[1])\
                    is None:
                        # If the color is not in the route, we add it.
                        self.route_output[available_color] = ColorInterpolator

        # Expose the available colors.
        self.available_input_colors = self.route_input.keys()
        self.available_output_colors = self.route_output.keys()
    def on(self, input, output, **kwargs):
        assert len(input.split("-")) < 2 or len(output.split("-")) < 2,\
        "Input and output cannot both be colors."
        if len(input.split("-")) == 2:
            # The input is a color, so choose that color in the routing table.
            interpolator = self.route_input[input]
            # If the interpolator has the key "on", use that:
            if hasattr(interpolator, "on"):
                interpolator = interpolator.on
            return interpolator(input, output, **kwargs)
        elif len(output.split("-")) == 2:
            # The output is a color, so choose that color in the routing table.
            interpolator = self.route_output[output]
            # If the interpolator has the key "on", use that:
            if hasattr(interpolator, "on"):
                interpolator = interpolator.on
            return interpolator(input, output, **kwargs)
        print("Warning: neither input or output are colors.")
    def add_interpolator_first(self, interpolator):
        self.interpolators = [intepolator] + self.interpolators
        self.refresh()
    def add_interpolator_last(self, interpolator):
        self.interpolators = self.interpolators + [intepolator]
        self.refresh()
    def find_shortest_path(self, graph, start, end, path=[]):
        """Code from https://www.python.org/doc/essays/graphs/
        Returns the shortest path between nodes in a supplied graph.
        3/19/2018"""
        path = path + [start]
        if start == end:
            return path
        if not start in graph:
            return None
        shortest = None
        for node in graph[start]:
            if node not in path:
                newpath = self.find_shortest_path(graph, node, end, path)
                if newpath:
                    if not shortest or len(newpath) < len(shortest):
                        shortest = newpath
        return shortest
    @staticmethod
    def color_list_to_graphs(color_list):
        """Generates a dictionary of graphs from a list of colors.
        Returns a bidirectional graph under the key "bidirectional",
        A forward-traversing graph under the key "forward",
        A backward-traversing graph under the key "backward".

        Accepts a list of color tuples like ("B", "V").
        Assumes tuple is a directed edge from the first color to
        the second color."""
        #Create the graph dictionaries:
        bidirectional_graph = {}
        forward_facing_graph = {}
        backward_facing_graph = {}
        #Traverse forward from each band to the final band.
        for initial_band, final_band in color_list:
            if not initial_band in forward_facing_graph:
                bidirectional_graph[initial_band] = [final_band]
                forward_facing_graph[initial_band] = [final_band]
            else:
                bidirectional_graph[initial_band].append(final_band)
                forward_facing_graph[initial_band].append(final_band)
        #Create a graph, going in reverse.
        for final_band, initial_band in color_list:
            if not initial_band in backward_facing_graph:
                backward_facing_graph[initial_band] = [final_band]
            else:
                backward_facing_graph[initial_band].append(final_band)
            if not initial_band in bidirectional_graph:
                bidirectional_graph[initial_band] = [final_band]
            else:
                bidirectional_graph[initial_band].append(final_band)
        #Return a dictionary of color lists.
        return {"forward":forward_facing_graph,
                "backward":backward_facing_graph,
                "bidirectional":bidirectional_graph}

class BaseIsochroneInterpolator(object):
    """Abstract base class for isochronal calculators."""
    def load_data(self):
        raise NotImplementedError
    def __init__(self, from_tuple, to_tuple, subsample_N = None, tol = 1e-6,
                 output_unit = (None, None)):
        """Initializes an isochronal interpolator on the tuples provided as
        from_tuple to to_tuple.
        IE: from: ("logT", "logL") to ("logage", "mass")
        Available values are found in self.column_headers.

        subsample_N is an optional parameter specifying the number of points of
        the initial guess 2d array. Leaving it as None will use the entire grid
        to guess with a simple nearest neighbor method.

        tol refers to the tolerance for gradient estimation in
        scipy.interpolate.

        output_unit is a tuple of units corresponding to the output to_tuple."""
        # Set the units to output in
        self.output_unit = output_unit

        ### Set the default inputs and outputs according to the default unit
        ### of each column datatype.
        # Use the default units for the chosen input and output columns.
        self._default_input_unit =\
        (self.default_units[from_tuple[0]], self.default_units[from_tuple[1]])
        self._default_output_unit =\
        (self.default_units[to_tuple[0]], self.default_units[to_tuple[1]])

        ## If the output unit is unset, then assume that the default units
        ## are what's requested.

        if output_unit[0] is None:
            _output_unit_0 = self._default_output_unit[0]
        else:
            _output_unit_0 = output_unit[0]
        if output_unit[1] is None:
            _output_unit_1 = self._default_output_unit[1]
        else:
            _output_unit_1 = output_unit[1]
        self.output_unit = (_output_unit_0, _output_unit_1)

        for provided_column_name in from_tuple + to_tuple:
            assert provided_column_name in self.column_headers,\
            "Column not found. Available columns are: "+\
            repr(self.column_headers)
        self.points = self.load_data()
        # Create a sub-sampled population that provides the initial guess
        # efficiently, for scipy.optimize.fmin to use prior to
        # performing the full minimization.
        self.sample_points = self.subsample(self.points, N = subsample_N)
        # Save the tuples that correspond to from and to.
        self.from_tuple = from_tuple
        self.to_tuple = to_tuple

        # Find the indices corresponding to the desired input and output
        # In the interpolation.
        input_index_val1 = self.column_headers.index(self.from_tuple[0])
        input_index_val2 = self.column_headers.index(self.from_tuple[1])
        output_index_val1 = self.column_headers.index(self.to_tuple[0])
        output_index_val2 = self.column_headers.index(self.to_tuple[1])

        ### Create the interpolants:
        # Interpolator1:
        self.interpolator1 =\
        scipy.interpolate.\
        CloughTocher2DInterpolator(list(zip(self.points.T[input_index_val1],
                                            self.points.T[input_index_val2])),
                                   self.points.T[output_index_val1],
                                   tol = tol)
        # Interpolator2:
        self.interpolator2 =\
        scipy.interpolate.\
        CloughTocher2DInterpolator(list(zip(self.points.T[input_index_val1],
                                            self.points.T[input_index_val2])),
                                   self.points.T[output_index_val2],
                                   tol = tol)
    @classmethod
    def subsample(self, points, N = None):
        """Creates a random subsample of points of size N"""
        if N is None:
            N = len(points)
        elif N > len(points):
            N = len(points)
        sorting_list = list(range(len(points)))
        random.shuffle(sorting_list)
        return points[sorting_list[0:N]]
    def get_guess(self, value1, value2,
                  value1_prior = lambda points: 1.,
                  value2_prior = lambda points: 1.):
        """Gets an initial guess to populate the solver"""
        if not hasattr(value1, "__len__"):
            len_value1 = 0
        else:
            len_value1 = len(value1)
        if not hasattr(value2, "__len__"):
            len_value2 = 0
        else:
            len_value2 = len(value2)
        assert len_value1 == len_value2,\
        "Arrays or values passed not of same dimension!"

        # Find the indices corresponding to the desired input and output
        # In the interpolation.
        input_index_val1 = self.column_headers.index(self.from_tuple[0])
        input_index_val2 = self.column_headers.index(self.from_tuple[1])
        output_index_val1 = self.column_headers.index(self.to_tuple[0])
        output_index_val2 = self.column_headers.index(self.to_tuple[1])
        ### Obtain a fast initial guess for each value1, value2.

        #Polymorphism support: if value1 and value2 by extension are scalars:
        if len_value1 < 1:
            chisq_wo_sigma =\
            np.square((value1 - self.sample_points.T[input_index_val1])) +\
            np.square((value2 - self.sample_points.T[input_index_val2]))
            # Obtain the log likelihood:
            with np.errstate(divide='ignore'):
                ln_likelihood =\
                np.log(value1_prior(self.sample_points))+\
                np.log(value2_prior(self.sample_points))+\
                (-.5*chisq_wo_sigma)
                # This method produces infinities, so turn these into NaN's
                ln_likelihood = np.where(np.isfinite(ln_likelihood),
                                         ln_likelihood,
                                         np.zeros(len(ln_likelihood)) + np.NaN)
            #Find the highest likelihood:
            guess_output_val1 =\
            self.sample_points[np.nanargmax(ln_likelihood)][output_index_val1]
            guess_output_val2 =\
            self.sample_points[np.nanargmax(ln_likelihood)][output_index_val2]
            output_vals = (guess_output_val1, guess_output_val2)
        else:
            guess_output_val1 = np.zeros(len_value1) + np.nan
            guess_output_val2 = np.zeros(len_value2) + np.nan
            for val_index in range(len_value1):
                chisq_wo_sigma =\
                np.square((value1[val_index] -\
                           self.sample_points.T[input_index_val1])) +\
                np.square((value2[val_index] -\
                           self.sample_points.T[input_index_val2]))
                # Obtain the log likelihood:
                with np.errstate(divide='ignore'):
                    ln_likelihood =\
                    np.log(value1_prior(self.sample_points))+\
                    np.log(value2_prior(self.sample_points))+\
                    (-.5*chisq_wo_sigma)
                    # This method produces infinities, so turn these into NaN's
                    ln_likelihood = np.where(np.isfinite(ln_likelihood),
                                             ln_likelihood,
                                             np.zeros(len(ln_likelihood)) +\
                                             np.NaN)
                #Find the highest likelihood:
                guess_output_val1[val_index] =\
                self.sample_points[np.nanargmax(ln_likelihood)][output_index_val1]
                guess_output_val2[val_index] =\
                self.sample_points[np.nanargmax(ln_likelihood)][output_index_val2]
            output_vals = (guess_output_val1, guess_output_val2)
        return output_vals
    def _evaluate(self, value1, value2,
                  value1_prior = lambda points: 1.,
                  value2_prior = lambda points: 1.,
                  debug = True):
        """Evalaute the 2D interpolated method with Scipy's
        CloughTocher2DInterpolator"""
        ### Polymorphism check.
        # First thing we check for is if a single error has been given
        # for each of the parameters:
        if not hasattr(value1, "__len__"):
            len_value1 = 0
        else:
            len_value1 = len(value1)
        if not hasattr(value2, "__len__"):
            len_value2 = 0
        else:
            len_value2 = len(value2)
        assert len_value1 == len_value2,\
        "Arrays or values passed not of same dimension!"

        # Find the indices corresponding to the desired input and output
        # In the interpolation.
        input_index_val1 = self.column_headers.index(self.from_tuple[0])
        input_index_val2 = self.column_headers.index(self.from_tuple[1])
        output_index_val1 = self.column_headers.index(self.to_tuple[0])
        output_index_val2 = self.column_headers.index(self.to_tuple[1])

        output_value =\
        (self.interpolator1(value1, value2),
         self.interpolator2(value1, value2))

        return output_value
    def evaluate(self, value1, value2, *args, **kwargs):
        """Evaluate and interpolate a given array or value.
        Modified to accept two values for 2D interpolation purposes."""
        if isinstance(value1, u.Quantity):
            # If so, set it.
            value1 = value1.to(self._default_input_unit[0]).value
        if isinstance(value2, u.Quantity):
            # If so, set it.
            value2 = value2.to(self._default_input_unit[1]).value
        ### Polymorphism:
        ### Check if additional arguments are given. If so, we need
        ### to convert each input value systematically.
        if len(args) > 0:
            for index in range(len(args)):
                # Check to see if the values passed have a dimensional component
                if isinstance(args[index], u.Quantity):
                    #Set the dimensional component for each value.
                    args[index] =\
                    args[index].to(self._default_input_unit[index+1]).value
            if len(kwargs.keys()) > 0:
                # Check whether kwargs is used as a key. (TODO: replace)
                if "kwargs" in kwargs.keys():
                    output = self._evaluate(value1, value2, *args)
                else:
                    output = self._evaluate(value1, value2, *args, **kwargs)
            else:
                output = self._evaluate(value1, value2, *args)
        else:
            if len(kwargs.keys()) > 0:
                # Check whether kwargs is used as a key. (TODO: replace)
                if "kwargs" in kwargs.keys():
                    output = self._evaluate(value1, value2)
                else:
                    output = self._evaluate(value1, value2, **kwargs)
            else:
                output = self._evaluate(value1, value2)
        # Convert from the default output unit to the specified output unit
        output_tuple =\
        ((self.output_unit[0]/self._default_output_unit[0]).\
        in_units(u.dimensionless_unscaled)*output[0],
        (self.output_unit[1]/self._default_output_unit[1]).\
        in_units(u.dimensionless_unscaled)*output[1])
        ## Another possibility, though it will not error if there is a
        ## problem with units:
        #output *= (self.output_unit/self._default_output_unit).\
        #decompose().scale
        return output_tuple
    def __call__(self, value1, value2, *args, **kwargs):
        """Allows calling the class-level object with evaluate's arguments.
        Returns the values with proper units attached.
        To avoid returning the units as well, use evaluate()."""
        evaluated_value = self.evaluate(value1, value2, *args, **kwargs)
        output_tuple =\
        (self.output_unit[0]*evaluated_value[0],
         self.output_unit[1]*evaluated_value[1])
        return output_tuple
