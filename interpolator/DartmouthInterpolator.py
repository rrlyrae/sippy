"""
sippy.interpolator.DartmouthInterpolator:
Interpolator for the Dartmouth 2014 isochrones.

Author: Lyra Cao
4/12/2018
"""

import numpy as np
from astropy import units as u
import re, os
from sippy.interpolator.BaseInterpolator import *
import random

class DartmouthIsochroneInterpolator(BaseIsochroneInterpolator):
    """Performs 2D interpolation on Dartmouth"""
    directory = "dartmouth_tracks/"
    default_units = \
    {"logage": u.dimensionless_unscaled,
    #"logage": u.dex(u.yr),
     "mass": u.Msun,
     #"logT": u.dex(u.K),
     #"logL": u.dex(u.Lsun)
     "logT": u.dimensionless_unscaled,
     "logL": u.dimensionless_unscaled}
    # The following column headers are from load_data, ordered.
    column_headers = ["logage", "mass", "logT", "logL"]
    @classmethod
    def load_data(self):
        """Dartmouth tracks correspond to 375,286 points in isochrone space."""
        directory = os.path.dirname(os.path.abspath(__file__))\
        + "/Data/" + self.directory
        points = []
        files = [f for f in os.listdir(directory) if\
                 os.path.isfile(directory+f)]
        for filename in files:
            with open(directory+filename, "r") as f:
                isotxt = f.read()
                if len(isotxt) > 0:
                    curr_mass = float(isotxt.split("M=")[1].split(" X=")[0])
                    raw_points = np.genfromtxt(directory+filename,
                                               usecols = (0,1,3)).tolist()
                    for point in raw_points:
                        points.append([np.log10(point[0]),
                                       curr_mass,
                                       point[1],
                                       point[2]])
        return np.array(points)
