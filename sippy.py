"""
SIPPY: Stellar Interpolation Package for PYthon
Author: Lyra Cao
A simple software package to interpolate freely between stellar parameters.
3/9/2018
"""

import interpolator
import calculator
