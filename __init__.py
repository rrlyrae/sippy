__version__ = "0.0.1"

__all__ = ["interpolator", "calculator"]
from sippy import interpolator
from sippy import calculator

# Debug:
from sippy import example
