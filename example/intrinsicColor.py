import matplotlib.pyplot as plt
import numpy as np
from astropy import units as u
from sippy.interpolator.BaseInterpolator import *
from sippy.interpolator.PecautMamajekInterpolator import *

def plot_SpT_Teff_PM13(interpolation_kind="linear"):
    """An example usage of a fallback SpT-Teff interpolation."""
    PecautMamajekDwarf =\
    PecautMamajekDwarfInterpolator("SpT", "Teff",
                                   interpolation_kind=interpolation_kind)
    PecautMamajekYoung =\
    PecautMamajekYoungInterpolator("SpT", "Teff",
                                   interpolation_kind=interpolation_kind)
    PecautMamajekExtendedDwarf =\
    PecautMamajekExtendedDwarfInterpolator("SpT", "Teff",
                                           interpolation_kind=\
                                           interpolation_kind)
    PM_Young_Extended =\
    MergedFallbackInstantiatedInterpolator([PecautMamajekYoung,
                                            PecautMamajekExtendedDwarf])
    xs = np.linspace(0.,90.,500)

    # Three subplots sharing both x/y axes
    f, (ax1, ax2) = plt.subplots(2, sharex=True, sharey=True)
    ax1.plot(xs, PecautMamajekDwarf.evaluate(xs),
             label="Dwarf",
             lw = 2, color = "red")
    ax1.plot(xs, PecautMamajekExtendedDwarf.evaluate(xs),
             label="ExtendedDwarf",
             lw = 2, color = "blue", linestyle="dotted")
    ax1.plot(xs, PecautMamajekYoung.evaluate(xs),
             label="$5-30 \\, [Myr]$",
             lw = 2, color = "orange")
    ax2.plot(xs, PM_Young_Extended.evaluate(xs),
             label="$5-30 \\, [Myr]$ + ExtendedDwarf",
             lw = 2, color = "black")
    ax1.set_title('Merged Color Table Interpolator Demo')
    # Fine-tune figure; make subplots close to each other and hide x ticks for
    # all but bottom plot.
    f.subplots_adjust(hspace=0)
    plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)

    ax2.set_xlabel("Numerical SpT")
    for ax in [ax1, ax2]:
        ax.set_ylabel("Temperature $[K]$")
        ax.semilogy()
        ax.legend()
    plt.show()
