"""
sippy.calculator.ColorCalculator:
Python classes and methods for calculating stellar parameters with colors.

Philosophically, a Calculator is different from an Interpolator because it
performs some kind of nontrivial calculation. That being said, there are
Interpolators in this Calculator file because there are components that only
do interpolation that belong here.
"""

from sippy.calculator.BaseCalculator import *
from sippy.interpolator.BaseInterpolator import *
from sippy.interpolator.PecautMamajekInterpolator import \
PecautMamajekDwarfInterpolator,\
PecautMamajekYoungInterpolator,\
PecautMamajekExtendedDwarfInterpolator
from sippy.interpolator.WhitneyInterpolator import WhitneyInterpolator
from sippy.interpolator.MathisInterpolator import MathisInterpolator
import os
import numpy as np
import scipy
import scipy.interpolate
import matplotlib.pyplot as plt
from astropy import units as u
import itertools as it

class PhotometricSystem(object):
    """A system of photometric bands with filter abbreviations.
    Loads default data from calculator/Data/photometric_bands.dat
    Which is a tab-separated file with filter abbreviations and effective
    wavelengths.

    The photometric system can be modified in place with methods in this
    class."""
    # Define the bands for the photometric system:
    bands = {}
    filename = "photometric_bands.dat"
    # The input file uses tab to delimit entries, so denote that:
    delimiter = "\t"
    # Define the datatypes:
    dtype = ["|S10", float]

    # Default unit is set by the data file:
    _default_unit = u.micron
    def __init__(self):
        filename = os.path.dirname(os.path.abspath(__file__))\
                 + "/Data/" + self.filename
        data_bands = np.genfromtxt(filename, comments = "#",
                                   dtype=self.dtype)
        for data_band in data_bands:
            # Set the band abbreviation and effective wavelength dictionary
            # in self.bands.
            self.bands[data_band[0].decode("utf-8")] =\
            data_band[1]
    def set_band(self, band, lambda_eff):
        """Set the band named "band"'s effective wavelength to lambda_eff.
        Creates this band if it doesn't already exist."""
        self.bands[band] = lambda_eff
    def remove_band(self, band):
        """Removes a band from this photometric system."""
        self.bands.pop(band, 0)
    def __call__(self, band):
        """Returns the effective wavelength of the band(s) requested, or
        a KeyError if the band is not loaded in the PhotometricSystem.

        A neat application of this __call__ is to get a sorted list of bands
        with the following code:

        ps = sippy.calculator.ColorCalculator.PhotometricSystem()
        bands = ["U", "V", "B", "J", "Ks", "H"]
        sorted(bands, key=ps)"""
        return self.__getitem__(band)*self._default_unit
    def __getitem__(self, band):
        """Returns the effective wavelength of the band(s) requested, or
        a KeyError if the band is not loaded in the PhotometricSystem."""
        if hasattr(band, "__len__") and (not isinstance(band, str)):
            #This is a list!
            bands = band
            return [self.bands[band] for band in bands]
        else:
            return self.bands[band]

class ColorMergeInterpolator(BaseInterpolator):
    """Takes a list (or single) color interpolator, finds the selected color,
    and merges them with passed MergingInterpolator logic.

    The default MergingInterpolator is MergedFallbackInstantiatedInterpolator,
    and under this scenario, the ColorInterpolator takes the first color
    interpolator under the specified color (i.e., B-V), and at runtime finds all
    values that are NaN and "falls back" through the available list of
    color interpolators.

    Accepts "SpT" input at runtime."""
    #The input value for SpT actually isn't decided by ColorCalculator
    #because it is handled entirely by the ColorInterpolator(s).
    _default_input_unit = u.dimensionless_unscaled
    #The output unit is in mags.
    _default_output_unit = u.mag
    def __init__(self,
                 color,
                 PhotometricSystem = PhotometricSystem(),
                 ColorInterpolators = [PecautMamajekYoungInterpolator,
                 PecautMamajekDwarfInterpolator,
                 PecautMamajekExtendedDwarfInterpolator],
                 MergingInterpolator = MergedFallbackInstantiatedInterpolator,
                 interpolation_kind = "linear",
                 output_unit = None):
        # Set the units to output in
        self.output_unit = output_unit
        ### If output unit is unset, set it to the default.
        if output_unit is None:
            self.output_unit = self._default_output_unit
        """Accepts a color (i.e., "B-V"), PhotometricSystem,
        ColorInterpolators, and MergingInterpolator."""
        ### Initialize the base color interpolators.
        self.interpolator = None
        # Save the color:
        self.color = color
        # Save the photometric system:
        self.PhotometricSystem = PhotometricSystem
        # Save the merging interpolator, which is a base interpolator
        # that accepts multiple interpolators and the submethod:
        # add_interpolator_last.
        self.MergingInterpolator = MergingInterpolator
        # Split the color into the constituent photometric bands.
        color_tuple = color.split("-")
        # Save the color tuple:
        self.color_tuple = color_tuple
        assert len(color_tuple) == 2, "Invalid color length chosen."
        ### Generate the color interpolators.
        self.generateColorInterpolators(ColorInterpolators,
                                        interpolation_kind = interpolation_kind)
    def _has_interpolator(self):
        return self.interpolator != None
    def _evaluate(self, spts):
        """Returns the requested intrinsic color for a given SpT value."""
        if self.interpolator is not None:
            intrinsic_color_values = self.interpolator.evaluate(spts)
        else:
            intrinsic_color_values = np.zeros(len(spts))+np.nan
        return intrinsic_color_values
    def generateColorInterpolators(self, ColorInterpolators,
                                   interpolation_kind = "linear"):
        """Generates ColorInterpolators and saves the complete
        interpolator to self.interpolator."""
        #Find available colors in each interpolator programmatically.
        #NB: if ColorInterpolators is a single ColorInterpolator, we simply
        #Make a list with one element and put it in there.
        if not hasattr(ColorInterpolators, "__len__"):
            ColorInterpolators = [ColorInterpolators]
        for PreColorInterpolator in ColorInterpolators:
            # For compatibility purposes, some subclasses of ColorInterpolators
            # have a method called "on" that is the equivalent of __init__
            # for most ColorInterpolators.
            # If a subclass has "on" defined, we should use "on" instead.
            # In all cases where subclasses have "on" defined, they should
            # have an internal list of interpolators.
            # This particular function is concerned with the available_colors
            # of the input interpolator.
            if hasattr(PreColorInterpolator, "on"):
                # If the ColorInterpolator has available_colors defined at
                # the class level, then we can simply look for it:
                if hasattr(PreColorInterpolator, "available_colors"):
                    AvailableColors = PreColorInterpolator.available_colors
                else:
                    if hasattr(PreColorInterpolator,
                               "available_output_colors"):
                        AvailableColors =\
                        PreColorInterpolator.available_output_colors
                ColorInterpolator = PreColorInterpolator.on
            else:
                ColorInterpolator = PreColorInterpolator
                AvailableColors = PreColorInterpolator.available_colors
            color_list = []
            #Grab all available bands by splitting each column_header entry
            #by "-" and checking which ones have two items in the resulting
            #tuple.
            for interpolator_color in AvailableColors:
                interpolator_color_tuple = interpolator_color.split("-")
                if len(interpolator_color_tuple) == 2:
                    color_list.append(interpolator_color_tuple)
            #Create the dictionary of graphs corresponding
            #to the directed traversals from photometric band to band.
            graph_dictionary = self.color_list_to_graphs(color_list)
            #Find the shortest path between the colors chosen for the Av.
            #The first element of the tuple is the start band, while the
            #Second is the last.
            path = self.find_shortest_path(graph_dictionary["bidirectional"],
                                           self.color_tuple[0],
                                           self.color_tuple[1])
            #Initialize the interpolator for this color sequence:
            interpolator = None
            #This path needs to be converted into a series of colors and
            #positive or negative coefficients.
            if path is not None:
                if len(path) > 1:
                    for index in range(len(path) - 1):
                        #Look at index, index + 1 as a single color
                        #(i.e., band pair.)
                        band_1 = path[index]
                        band_2 = path[index + 1]
                        #This is either a positive-facing path or negative
                        #facing one.
                        #Check by looking at the directional graphs:
                        if band_1 in graph_dictionary["forward"]:
                            if band_2 in graph_dictionary["forward"][band_1]:
                                #The band_1-band_2 color exists.
                                desired_color = "-".join([band_1, band_2])
                                if interpolator is None:
                                    interpolator =\
                                    ColorInterpolator("SpT",
                                                      desired_color,
                                                      interpolation_kind =\
                                                      interpolation_kind)
                                else:
                                    interpolator =\
                                    interpolator + \
                                    ColorInterpolator("SpT",
                                                      desired_color,
                                                      interpolation_kind =\
                                                      interpolation_kind)
                        if band_1 in graph_dictionary["backward"]:
                            if band_2 in graph_dictionary["backward"][band_1]:
                                #The band_2-band_1 color exists.
                                desired_color = "-".join([band_2, band_1])
                                if interpolator is None:
                                    temp_interp =\
                                    ColorInterpolator("SpT",
                                                      desired_color,
                                                      interpolation_kind =\
                                                      interpolation_kind)
                                    interpolator =\
                                    NegativeInterpolator(temp_interp)
                                else:
                                    interpolator =\
                                    interpolator - \
                                    ColorInterpolator("SpT",
                                                      desired_color,
                                                      interpolation_kind =\
                                                      interpolation_kind)
                        else:
                            #Band not found! Whatever.
                            pass
                #We now have the correct interpolator for this particular
                #ColorInterpolator sequence.
                #We need to merge it or set it as the output interpolator:
                if self.interpolator is None:
                    #If the interpolator has not been set,
                    self.interpolator =\
                    self.MergingInterpolator([interpolator])
                else:
                    #The interpolator has been set, so we need to merge it.
                    self.interpolator.add_interpolator_last(interpolator)
    def find_shortest_path(self, graph, start, end, path=[]):
        """Code from https://www.python.org/doc/essays/graphs/
        Returns the shortest path between nodes in a supplied graph.
        3/19/2018"""
        path = path + [start]
        if start == end:
            return path
        if not start in graph:
            return None
        shortest = None
        for node in graph[start]:
            if node not in path:
                newpath = self.find_shortest_path(graph, node, end, path)
                if newpath:
                    if not shortest or len(newpath) < len(shortest):
                        shortest = newpath
        return shortest
    @staticmethod
    def color_list_to_graphs(color_list):
        """Generates a dictionary of graphs from a list of colors.
        Returns a bidirectional graph under the key "bidirectional",
        A forward-traversing graph under the key "forward",
        A backward-traversing graph under the key "backward".

        Accepts a list of color tuples like ("B", "V").
        Assumes tuple is a directed edge from the first color to
        the second color."""
        #Create the graph dictionaries:
        bidirectional_graph = {}
        forward_facing_graph = {}
        backward_facing_graph = {}
        #Traverse forward from each band to the final band.
        for initial_band, final_band in color_list:
            if not initial_band in forward_facing_graph:
                bidirectional_graph[initial_band] = [final_band]
                forward_facing_graph[initial_band] = [final_band]
            else:
                bidirectional_graph[initial_band].append(final_band)
                forward_facing_graph[initial_band].append(final_band)
        #Create a graph, going in reverse.
        for final_band, initial_band in color_list:
            if not initial_band in backward_facing_graph:
                backward_facing_graph[initial_band] = [final_band]
            else:
                backward_facing_graph[initial_band].append(final_band)
            if not initial_band in bidirectional_graph:
                bidirectional_graph[initial_band] = [final_band]
            else:
                bidirectional_graph[initial_band].append(final_band)
        #Return a dictionary of color lists.
        return {"forward":forward_facing_graph,
                "backward":backward_facing_graph,
                "bidirectional":bidirectional_graph}

class ColorMergeAvCalculator(ColorMergeInterpolator, BaseCalculator):
    """Calculates Av for a given color (ie. J-Ks).

    Takes a list (or single) color interpolator, finds the selected color,
    and merges them with passed MergingInterpolator logic.

    The default MergingInterpolator is MergedFallbackInstantiatedInterpolator,
    and under this scenario, the ColorInterpolator takes the first color
    interpolator under the specified color (i.e., B-V), and at runtime finds all
    values that are NaN and "falls back" through the available list of
    color interpolators.

    Uses these as the colors to use through the calculations to Av."""
    _inputs = ["photometry", "spt", "veiling"]
    _outputs = ["Av"]
    #The input unit is in mags.
    _default_input_unit = u.mag
    #The output unit is in mags.
    _default_output_unit = u.mag
    def __init__(self,
                 color,
                 PhotometricSystem = PhotometricSystem(),
                 ColorInterpolators = [PecautMamajekYoungInterpolator,
                 PecautMamajekDwarfInterpolator,
                 PecautMamajekExtendedDwarfInterpolator],
                 ExtinctionInterpolator =\
                 MergedFallbackInstantiatedInterpolator([
                         WhitneyInterpolator(interpolation_kind = "linear"),
                         MathisInterpolator(interpolation_kind = "linear")
                     ]),
                 MergingInterpolator = MergedFallbackInstantiatedInterpolator,
                 interpolation_kind = "linear",
                 output_unit = None):
        """Accepts a color (i.e., "B-V"), PhotometricSystem,
        ColorInterpolators, ExtinctionInterpolator, and MergingInterpolator.

        Usage: call evaluate() with values for a color (i.e., photometric
        values for "B-V") and SpT's.

        Generates color interpolators for use in calculating Av's."""
        # Set the units to output in
        self.output_unit = output_unit
        ### If output unit is unset, set it to the default.
        if output_unit is None:
           self.output_unit = self._default_output_unit
        ### Initialize the base color interpolators.
        self.interpolator = None
        # Save the color:
        self.color = color
        # Save the photometric system:
        self.PhotometricSystem = PhotometricSystem
        # Save the extinction interpolator:
        self.ExtinctionInterpolator = ExtinctionInterpolator
        # Save the merging interpolator, which is a base interpolator
        # that accepts multiple interpolators and the submethod:
        # add_interpolator_last.
        self.MergingInterpolator = MergingInterpolator
        # Split the color into the constituent photometric bands.
        color_tuple = color.split("-")
        # Save the color tuple:
        self.color_tuple = color_tuple
        assert len(color_tuple) == 2, "Invalid color length chosen."
        ### Generate the color interpolators.
        self.generateColorInterpolators(ColorInterpolators,
                                        interpolation_kind = interpolation_kind)
    def _evaluate(self, spts, color_values):
        """Returns a list of Avs for an input list of values for a color,
        (i.e., photometry for B - photometry for V), and SpT's."""
        #Find the wavelengths corresponding to the photometric bands in the
        #color:
        wavelength_1 = self.PhotometricSystem(self.color_tuple[0])
        wavelength_2 = self.PhotometricSystem(self.color_tuple[1])
        if self.interpolator is not None:
            Av = (np.array(color_values) -\
                  self.interpolator.evaluate(spts))/\
            (self.ExtinctionInterpolator(wavelength_1) -\
             self.ExtinctionInterpolator(wavelength_2))
        else:
            Av = np.nan + color_values
        return Av

class ColorSEDAvCalculator(ColorMergeInterpolator, BaseCalculator):
    """Calculates the Av for a given series of photometries and
    ColorInterpolators with the SED fitting technique."""
    _inputs = ["photometry", "spt", "veiling"]
    _outputs = ["Av"]
    #The input unit is in mags.
    _default_input_unit = u.mag
    #The output unit is in mags.
    _default_output_unit = u.mag
    def __init__(self,
                 PhotometricSystem = PhotometricSystem(),
                 ColorInterpolators = [PecautMamajekYoungInterpolator,
                 PecautMamajekDwarfInterpolator,
                 PecautMamajekExtendedDwarfInterpolator],
                 ExtinctionInterpolator =\
                 MergedFallbackInstantiatedInterpolator([
                         WhitneyInterpolator(interpolation_kind = "linear"),
                         MathisInterpolator(interpolation_kind = "linear")
                     ]),
                 MergingInterpolator = MergedFallbackInstantiatedInterpolator,
                 interpolation_kind = "linear",
                 output_unit = None,
                 pivot_band = "Y",
                 required_dof = 2):
        """Accepts a PhotometricSystem,
        ColorInterpolators, ExtinctionInterpolator, and MergingInterpolator.

        Usage: call evaluate() with values for a series of photometries
        (i.e., photometric values for "B", "V") and SpT's. Returns Av_SED.

        Generates color interpolators for use in calculating Av's.

        pivot_band is the name of a band defined in PhotometricSystem
        that is the preferred band wavelength to transform colors in
        ColorInterpolators to photometries. If pivot_band doesn't exist in
        the attached ColorInterpolators, or if it doesn't exist in the provided
        photometries, the code will look for an adjacent photometry in
        wavelength space, iterating until it finds one.

        i.e., pivot_band = "Y" at 1.035 microns, the code might choose
        "J" if "Y" doesn't exist, because it's at 1.25 microns. Then,
        It will use the J band to define ColorInterpolator's colors.

        NB: SED fitting is not possible with just one point. It must have
        At least two points in common, but three or more is better. The
        degrees of freedom is defined as the number of photometry points minus
        one.

        The parameter "required_dof" is used to set the number of degrees of
        freedom required before an Av is reported at all. Otherwise, it will
        report numpy.NaN."""
        # Set the units to output in
        self.output_unit = output_unit
        ### If output unit is unset, set it to the default.
        if output_unit is None:
           self.output_unit = self._default_output_unit
        ### Initialize the base color interpolators.
        self.interpolator = None
        # Save the photometric system:
        self.PhotometricSystem = PhotometricSystem
        # Save the color interpolator:
        self.ColorInterpolators = ColorInterpolators
        # Save the extinction interpolator:
        self.ExtinctionInterpolator = ExtinctionInterpolator
        # Save the merging interpolator, which is a base interpolator
        # that accepts multiple interpolators and the submethod:
        # add_interpolator_last.
        self.MergingInterpolator = MergingInterpolator
        # Save interpolation_kind:
        self.interpolation_kind = interpolation_kind
        # Save pivot_band:
        self.pivot_band = pivot_band
        # Save the required DOF:
        self.required_dof = required_dof
    def _evaluate(self, spts, av_tol = 1e-2, av_max = 10., chisq_max = 10.,
                  **photometries):
        """Evaluates Av_SED with a given series of SpTs and photometries.
        av_tol is the bounding box over which chi-sq minimization is done.
        """
        # Make sure input SpT's are in a Numpy array.
        spts = np.array(spts)
        ### Sort photometries according to how close they are to the desired
        ### pivot band.
        photometry_list = photometries.keys()
        photometry_list = sorted(photometry_list, key=\
                                 lambda x:\
                                 abs(self.PhotometricSystem(x) - \
                                     self.PhotometricSystem(self.pivot_band)))
        # Prepare a dictionary that stores the intrinsic photometries,
        # initially populated with NaN's.
        intrinsic_spectrum = {}
        # Create two lists: one called:
        # "attempt_index", which contains booleans that state whether a given
        # set of data will attempt calculation with its colors.
        # Initially, it is all set to True, but it will be set to False once
        # enough spectral types .
        attempt_index = np.array([True]*len(list(photometries.values())[0]),
                                 dtype = "bool")
        self._log_color_used = np.array(['']* len(attempt_index), dtype="<U2")
        for photometric_band in photometries:
            intrinsic_spectrum[photometric_band] =\
            np.zeros(len(photometries[photometric_band]))+np.nan
            #Make sure each photometry is a np.array.
            photometries[photometric_band] =\
            np.array(photometries[photometric_band])
        # Iterate over the photometry list in order to obtain colors:
        for color_second in photometry_list:
            for color_first in photometry_list:
                if color_first != color_second:
                    # Create a color!
                    color = [color_first, color_second]
                    # Create a color interpolator for this color:
                    # The color name is "color_first - color_second"
                    color_interp =\
                    ColorMergeInterpolator("-".join(color),
                                           PhotometricSystem =\
                                           self.PhotometricSystem,
                                           ColorInterpolators =\
                                           self.ColorInterpolators,
                                           MergingInterpolator =\
                                           self.MergingInterpolator,
                                           interpolation_kind =\
                                           self.interpolation_kind,
                                           output_unit = self.output_unit)
                    if color_interp._has_interpolator():
                        # Find the indices for which photometries is not NaN.
                        # The intrinsic spectrum should only be populated if:
                        # 1. the intrinsic spectrum at this band is NaN.
                        # 2. the photometries is not NaN
                        # Obtain a spectrum by adding the photometry for
                        # "color_second" to the intrinsic color,
                        # color_first - color_second:
                        condition = np.logical_and(attempt_index,\
                        np.logical_and(\
                                np.isnan(intrinsic_spectrum[color_first]),
                                np.logical_not(np.isnan(photometries\
                                                        [color_second]))))
                        intrinsic_spectrum[color_first][condition] =\
                        color_interp.evaluate(spts[condition]) +\
                        photometries[color_second][condition]
            # Set the fixed spectral point:
            #intrinsic_spectrum[color_second] = photometries[color_second]
            # Set the _log_color_used:
            self._log_color_used[attempt_index] =\
            [color_second]*np.sum(attempt_index)
            # Try again (give an attempt of True) if the number of
            # DOF's is less than the required degrees of freedom.
            for index in range(len(intrinsic_spectrum[color_second])):
                attempt_index[index] =\
                np.sum([np.logical_not(\
                np.isnan(intrinsic_spectrum[intrinsic_color][index]))\
                for intrinsic_color in intrinsic_spectrum]) < self.required_dof\
                -1
            # Unset the _log_color_used parameter for spectra deemed unfit.
            self._log_color_used[attempt_index] = ['']*np.sum(attempt_index)
            # Unset the values for all photometries for unfit spectra:
            for temp_color in intrinsic_spectrum:
                intrinsic_spectrum[temp_color][attempt_index] =\
                [np.NaN]*np.sum(attempt_index)
        # Set the fixed spectral point:
        for index in range(len(self._log_color_used)):
            #print(index,[x[index] for x in photometries.values()]) #DEBUG
            color_used = self._log_color_used[index]
            if len(color_used) > 0:
                intrinsic_spectrum[color_used][index] =\
                photometries[color_used][index]
        # Now we have an intrinsic_spectrum, so we need to calculate an A_v
        # And its associated A_lambda/A_v in order to proceed.

        # The pivot point is still the first entry of photometry_list,
        # So we can add A(band) - A(band_pivot) to introduce a zero-point offset
        # That fixes the pivot point to the observed photometries.
        Alambda_div_Av_dictionary =\
        dict(zip(photometry_list,
                 self.ExtinctionInterpolator(\
                 self.PhotometricSystem(photometry_list))))
        ### Now, we simply construct the Av reasonable left and right
        ### golden section search.
        ### However, there might not be a minimum chisq for the Av.
        ### The termination condition used should just be that the bounding
        ### size of the Av box, in Av space, is smaller than some tolerance.
        ### This corresponds to \delta Av < tol
        #DEBUG: setting av_sig[band] to be a dictionary with artificial errors
        av_sig = {}
        for band in photometries:
            av_sig[band] = 1.
        #END DEBUG
        golden_ratio = 0.38197
        array_len = len(list(photometries.values())[0])
        Av_guess_a = np.zeros(array_len)
        Av_guess_c = np.zeros(array_len)+\
        av_max
        Av_guess_b = golden_ratio*(Av_guess_a + Av_guess_c)
        pivot_band = photometry_list[0]
        self.reduced_chisq = np.zeros(array_len) + np.nan
        # Iterate the golden section routine until we converge to an acceptably
        # small region.
        # Note: in some cases, a completely NaN series of photometries is given
        # That result should return a NaN reduced chisq.
        # Find the guess Av, ie the point b' in between the larger of the
        # intervals a-b, b-c. Denote b' as bp.
        Av_diff_ab = Av_guess_b - Av_guess_a
        Av_diff_bc = Av_guess_c - Av_guess_b
        Av_guess_bp = Av_guess_b +\
        golden_ratio*np.where(Av_diff_ab < Av_diff_bc,
                              Av_diff_bc,
                              Av_diff_ab)
        chisq_a = np.zeros(array_len)
        chisq_b = np.zeros(array_len)
        chisq_bp = np.zeros(array_len)
        chisq_c = np.zeros(array_len)
        num_bands = np.zeros(array_len)
        # Initialize the golden section routine, by calculating Avs for a,b,bp,c
        # and their associated chisq's.
        for band in photometries:

            band_chisq_a =\
            ((Av_guess_a*(Alambda_div_Av_dictionary[band] -\
                             Alambda_div_Av_dictionary[pivot_band])+\
            intrinsic_spectrum[band] - photometries[band])/\
             av_sig[band])**2

            band_chisq_b =\
            ((Av_guess_b*(Alambda_div_Av_dictionary[band] -\
                             Alambda_div_Av_dictionary[pivot_band])+\
            intrinsic_spectrum[band] - photometries[band])/\
            av_sig[band])**2

            band_chisq_bp =\
            ((Av_guess_bp*(Alambda_div_Av_dictionary[band] -\
                             Alambda_div_Av_dictionary[pivot_band])+\
            intrinsic_spectrum[band] - photometries[band])/\
            av_sig[band])**2

            band_chisq_c =\
            ((Av_guess_c*(Alambda_div_Av_dictionary[band] -\
                             Alambda_div_Av_dictionary[pivot_band])+\
            intrinsic_spectrum[band] - photometries[band])/\
            av_sig[band])**2

            #Have to check whether entries have NaN values. Only add non-NaN
            #values, and only increment num_bands for non-NaN values.

            condition_a = np.logical_not(np.isnan(band_chisq_a))
            condition_b = np.logical_not(np.isnan(band_chisq_b))
            condition_bp = np.logical_not(np.isnan(band_chisq_bp))
            condition_c = np.logical_not(np.isnan(band_chisq_c))

            condition_notnan = np.logical_and(np.logical_and(\
            np.logical_and(condition_a, condition_b),
            condition_c),
            condition_bp)

            chisq_a[condition_notnan] += band_chisq_a[condition_notnan]
            chisq_b[condition_notnan] += band_chisq_b[condition_notnan]
            chisq_bp[condition_notnan] += band_chisq_bp[condition_notnan]
            chisq_c[condition_notnan] += band_chisq_c[condition_notnan]

            num_bands[condition_notnan] += 1
        # Calculate the reduced chisq, setting values with DOF < required_dof
        # to NaN.
        condition_not_enough_dofs = num_bands < self.required_dof
        num_bands[condition_not_enough_dofs] = np.NaN
        chisq_a /= (num_bands - 1)
        chisq_b /= (num_bands - 1)
        chisq_bp /= (num_bands - 1)
        chisq_c /= (num_bands - 1)
        while np.max(Av_guess_c - Av_guess_a) > av_tol:
            ### In the golden section routine, a new triplet (a,b,c) is chosen
            ### To be either a,min(bp,b),max(bp,b), or min(bp,b),max(bp,b),c.
            ### This is chosen by comparing the reduced chisq's of a and c.
            condition_a_lt_c = chisq_a < chisq_c
            condition_b_lt_bp = Av_guess_b < Av_guess_bp
            chisq_min_b_bp = np.where(condition_b_lt_bp,
                                      chisq_b,
                                      chisq_bp)
            chisq_max_b_bp = np.where(condition_b_lt_bp,
                                      chisq_bp,
                                      chisq_b)
            Av_guess_min_b_bp = np.where(condition_b_lt_bp,
                                         Av_guess_b,
                                         Av_guess_bp)
            Av_guess_max_b_bp = np.where(condition_b_lt_bp,
                                         Av_guess_bp,
                                         Av_guess_b)
            # Calculate the Av guesses and the Reduced Chisq values:
            Av_guess_a = np.where(condition_a_lt_c,
                                  Av_guess_a,
                                  Av_guess_min_b_bp)
            chisq_a = np.where(condition_a_lt_c,
                               chisq_a,
                               chisq_min_b_bp)
            Av_guess_c = np.where(condition_a_lt_c,
                                  Av_guess_max_b_bp,
                                  Av_guess_c)
            chisq_c = np.where(condition_a_lt_c,
                               chisq_max_b_bp,
                               chisq_c)
            Av_guess_b = np.where(condition_a_lt_c,
                                  Av_guess_min_b_bp,
                                  Av_guess_max_b_bp)
            chisq_b = np.where(condition_a_lt_c,
                               chisq_min_b_bp,
                               chisq_max_b_bp)
            # A new b' point is now chosen:
            Av_diff_ab = Av_guess_b - Av_guess_a
            Av_diff_bc = Av_guess_c - Av_guess_b
            Av_guess_bp = Av_guess_b +\
            golden_ratio*np.where(Av_diff_ab < Av_diff_bc,
                                  Av_diff_bc,
                                  -Av_diff_ab)

            # Prepare to calculate the red_chisq of point b':
            num_bands = np.zeros(array_len)
            chisq_bp = np.zeros(array_len)
            for band in photometries:
                #Have to check whether entries have NaN values. Only add non-NaN
                #values, and only increment num_bands for non-NaN values.
                band_chisq_bp =\
                ((Av_guess_bp*(Alambda_div_Av_dictionary[band] -\
                                 Alambda_div_Av_dictionary[pivot_band])+\
                intrinsic_spectrum[band] - photometries[band])/\
                av_sig[band])**2
                condition_bp = np.logical_not(np.isnan(band_chisq_bp))
                chisq_bp[condition_bp] += band_chisq_bp[condition_bp]
                num_bands[condition_bp] += 1
            condition_not_enough_dofs = num_bands < self.required_dof
            num_bands[condition_not_enough_dofs] = np.NaN
            chisq_bp /= (num_bands - 1)
        ### Return the values with the lowest reduced chisq:
        lowest_reduced_chisq = np.zeros(array_len) + np.inf
        Av_SED = np.zeros(array_len) + np.nan
        for Av_guess, chisq in \
        zip([Av_guess_a, Av_guess_b, Av_guess_bp, Av_guess_c],
            [chisq_a, chisq_b, chisq_bp, chisq_c]):
            # Only replace lowest_reduced_chisq if chisq is non-NaN and
            # smaller!
            condition_not_nan = np.logical_not(np.isnan(chisq))
            condition_chisq_smaller = chisq < lowest_reduced_chisq
            condition_replace = np.logical_and(condition_not_nan,
                                               condition_chisq_smaller)
            # Replace the lowest reduced chisq with cases where the condition
            # is met!
            lowest_reduced_chisq[condition_replace] = chisq[condition_replace]
            Av_SED[condition_replace] = Av_guess[condition_replace]
        # Save the reduced chisq values.
        self._lowest_reduced_chisq = lowest_reduced_chisq
        chisq_cond = self._lowest_reduced_chisq < chisq_max
        return np.where(chisq_cond, Av_SED, np.zeros(array_len) + np.nan)

class ColorWeightedAvCalculator(ColorMergeInterpolator, BaseCalculator):
    """Calculates Av for a network of possible colors and weights them together.

    Takes a list (or single) color interpolator, finds the selected color,
    and merges them with passed MergingInterpolator logic.

    The default MergingInterpolator is MergedFallbackInstantiatedInterpolator,
    and under this scenario, the ColorInterpolator takes the first color
    interpolator under the specified color (i.e., B-V), and at runtime finds all
    values that are NaN and "falls back" through the available list of
    color interpolators.

    Uses these as the colors to use through the calculations to Av."""
    _inputs = ["photometry", "spt", "veiling"]
    _outputs = ["Av"]
    #The input unit is in mags.
    _default_input_unit = u.mag
    #The output unit is in mags.
    _default_output_unit = u.mag
    # Statistical weights for colors:
    weights = {}
    # Weights for calculating weighted average values of parameters.
    weights["Ic"]            = 1.0
    weights["Rc"]            = 1.0
    weights["V"]             = 0.9
    weights["J"]             = 0.7
    weights["H"]             = 0.3
    weights["Ks"]            = 0.2
    weights["default"]       = 0.
    def __init__(self,
                 color_combinations,
                 PhotometricSystem = PhotometricSystem(),
                 ColorInterpolators = [PecautMamajekYoungInterpolator,
                 PecautMamajekDwarfInterpolator,
                 PecautMamajekExtendedDwarfInterpolator],
                 ExtinctionInterpolator =\
                 MergedFallbackInstantiatedInterpolator([
                         WhitneyInterpolator(interpolation_kind = "linear"),
                         MathisInterpolator(interpolation_kind = "linear")
                     ]),
                 MergingInterpolator = MergedFallbackInstantiatedInterpolator,
                 interpolation_kind = "linear",
                 output_unit = None):
        """Accepts a list of colors --- a color combination, PhotometricSystem,
        ColorInterpolators, ExtinctionInterpolator, and MergingInterpolator.

        Usage: call evaluate() with SpT's and photometries.

        Generates color interpolators for use in calculating Av's."""
        # Set the units to output in
        self.output_unit = output_unit
        ### If output unit is unset, set it to the default.
        if output_unit is None:
           self.output_unit = self._default_output_unit
        # Save the color:
        # Save the photometric system:
        self.PhotometricSystem = PhotometricSystem
        # Save the extinction interpolator:
        self.ExtinctionInterpolator = ExtinctionInterpolator
        # Save the merging interpolator, which is a base interpolator
        # that accepts multiple interpolators and the submethod:
        # add_interpolator_last.
        self.MergingInterpolator = MergingInterpolator
         ### Initialize the base color interpolators.
        self.interpolators = {}
        for color in color_combinations:
            self.interpolators[color] =\
            ColorMergeAvCalculator(color,
            PhotometricSystem = PhotometricSystem,
            ColorInterpolators = ColorInterpolators,
            ExtinctionInterpolator = ExtinctionInterpolator,
            MergingInterpolator = MergingInterpolator,
            interpolation_kind = interpolation_kind,
            output_unit = output_unit)
    def _evaluate(self, spts, **photometries):
        """Returns a list of Avs for an input list of values for SpT's and
        photometries."""
        # Create possible color combinations as Band1-Band2:
        color_combinations =\
        ["-".join(color_tuple) for color_tuple in \
         it.combinations(sorted(photometries.keys(),
                                key=self.PhotometricSystem), 2)]
        Avs = {}
        weights = {}
        # Iterate over each color provided.
        for color in color_combinations:
            Avs[color] = self.interpolators[color].\
            evaluate(spts,
                     np.array(photometries[color.split("-")[0]]) -\
                     np.array(photometries[color.split("-")[1]]))
            weights[color] = np.zeros(len(Avs[color])) + np.NaN
            # Make sure only the non-NaN values for Avs are used.
            not_NaN = np.logical_not(np.isnan(Avs[color]))
            ### Designate multiplicative weights, ie: the weights
            ### for a single color are the multiplied weights
            ### for each of the photometric bands.
            weights[color][not_NaN] =\
            self.weights.get(color.split("-")[0],
                             self.weights["default"])*\
            self.weights.get(color.split("-")[1],
                             self.weights["default"])
        Av =\
        np.nansum([Avs[color]*weights[color] for color in color_combinations],
                  axis=0)/\
        np.nansum([weights[color] for color in color_combinations],
                  axis=0)
        return Av

class LogLCalculator(ColorMergeInterpolator, BaseCalculator):
    """Calculates LogL for photometries, spectral types, and distances."""
    def __init__(self,
                 color_combinations,
                 PhotometricSystem = PhotometricSystem(),
                 ColorInterpolators = [PecautMamajekYoungInterpolator,
                 PecautMamajekDwarfInterpolator,
                 PecautMamajekExtendedDwarfInterpolator],
                 ExtinctionInterpolator =\
                 MergedFallbackInstantiatedInterpolator([
                         WhitneyInterpolator(interpolation_kind = "linear"),
                         MathisInterpolator(interpolation_kind = "linear")
                     ]),
                 MergingInterpolator = MergedFallbackInstantiatedInterpolator,
                 interpolation_kind = "linear",
                 output_unit = None,
                 pivot_band = "Y",
                 required_dof = 2):
        """Accepts a list of colors --- a color combination, PhotometricSystem,
        ColorInterpolators, ExtinctionInterpolator, and MergingInterpolator.

        Usage: call evaluate() with SpT's and photometries.

        Generates color interpolators for use in calculating Av's."""
        # Set the units to output in
        self.output_unit = output_unit
        ### If output unit is unset, set it to the default.
        if output_unit is None:
           self.output_unit = self._default_output_unit
        ### Initialize the base color interpolators.
        self.interpolator = None
        # Save the photometric system:
        self.PhotometricSystem = PhotometricSystem
        # Save the color interpolator:
        self.ColorInterpolators = ColorInterpolators
        # Save the extinction interpolator:
        self.ExtinctionInterpolator = ExtinctionInterpolator
        # Save the merging interpolator, which is a base interpolator
        # that accepts multiple interpolators and the submethod:
        # add_interpolator_last.
        self.MergingInterpolator = MergingInterpolator
        # Save interpolation_kind:
        self.interpolation_kind = interpolation_kind
        # Save pivot_band:
        self.pivot_band = pivot_band
        # Save the required DOF:
        self.required_dof = required_dof
    def _evaluate(self, spts, av, dist, **photometries):
        """Returns a list of logL's for an input list of values for SpT's and
        photometries.

        Assume dist is given in units of parsecs."""
        if isinstance(dist, u.Quantity):
            # If so, set it.
            dist = dist.to(u.pc).value
        ### In order to generate a V-band photometry where it might not exist,
        ### Create an intrinsic spectrum:
        # Make sure input SpT's are in a Numpy array.
        spts = np.array(spts)
        ### Sort photometries according to how close they are to the desired
        ### pivot band.
        photometry_list = photometries.keys()
        photometry_list = sorted(photometry_list, key=\
                                 lambda x:\
                                 abs(self.PhotometricSystem(x) - \
                                     self.PhotometricSystem(self.pivot_band)))
        # Prepare a dictionary that stores the intrinsic photometries,
        # initially populated with NaN's.
        intrinsic_spectrum = {}
        # Create two lists: one called:
        # "attempt_index", which contains booleans that state whether a given
        # set of data will attempt calculation with its colors.
        # Initially, it is all set to True, but it will be set to False once
        # enough spectral types .
        attempt_index = np.array([True]*len(list(photometries.values())[0]),
                                 dtype = "bool")
        self._log_color_used = np.array(['']* len(attempt_index), dtype="<U2")
        for photometric_band in photometries:
            intrinsic_spectrum[photometric_band] =\
            np.zeros(len(photometries[photometric_band]))+np.nan
            #Make sure each photometry is a np.array.
            photometries[photometric_band] =\
            np.array(photometries[photometric_band])
        # Iterate over the photometry list in order to obtain colors:
        for color_second in photometry_list:
            for color_first in photometry_list:
                if color_first != color_second:
                    # Create a color!
                    color = [color_first, color_second]
                    # Create a color interpolator for this color:
                    # The color name is "color_first - color_second"
                    color_interp =\
                    ColorMergeInterpolator("-".join(color),
                                           PhotometricSystem =\
                                           self.PhotometricSystem,
                                           ColorInterpolators =\
                                           self.ColorInterpolators,
                                           MergingInterpolator =\
                                           self.MergingInterpolator,
                                           interpolation_kind =\
                                           self.interpolation_kind,
                                           output_unit = self.output_unit)
                    if color_interp._has_interpolator():
                        # Find the indices for which photometries is not NaN.
                        # The intrinsic spectrum should only be populated if:
                        # 1. the intrinsic spectrum at this band is NaN.
                        # 2. the photometries is not NaN
                        # Obtain a spectrum by adding the photometry for
                        # "color_second" to the intrinsic color,
                        # color_first - color_second:
                        condition = np.logical_and(attempt_index,\
                        np.logical_and(\
                                np.isnan(intrinsic_spectrum[color_first]),
                                np.logical_not(np.isnan(photometries\
                                                        [color_second]))))
                        intrinsic_spectrum[color_first][condition] =\
                        color_interp.evaluate(spts[condition]) +\
                        photometries[color_second][condition]
            # Set the fixed spectral point:
            #intrinsic_spectrum[color_second] = photometries[color_second]
            # Set the _log_color_used:
            self._log_color_used[attempt_index] =\
            [color_second]*np.sum(attempt_index)
            # Try again (give an attempt of True) if the number of
            # DOF's is less than the required degrees of freedom.
            for index in range(len(intrinsic_spectrum[color_second])):
                attempt_index[index] =\
                np.sum([np.logical_not(\
                np.isnan(intrinsic_spectrum[intrinsic_color][index]))\
                for intrinsic_color in intrinsic_spectrum]) < self.required_dof\
                -1
            # Unset the _log_color_used parameter for spectra deemed unfit.
            self._log_color_used[attempt_index] = ['']*np.sum(attempt_index)
            # Unset the values for all photometries for unfit spectra:
            for temp_color in intrinsic_spectrum:
                intrinsic_spectrum[temp_color][attempt_index] =\
                [np.NaN]*np.sum(attempt_index)
        # Set the fixed spectral point:
        for index in range(len(self._log_color_used)):
            #print(index,[x[index] for x in photometries.values()]) #DEBUG
            color_used = self._log_color_used[index]
            if len(color_used) > 0:
                intrinsic_spectrum[color_used][index] =\
                photometries[color_used][index]
        # Now we have an intrinsic_spectrum
        #
        bcv_interp = MergedFallbackInterpolator(self.ColorInterpolators)
        if hasattr(bcv_interp, "on"):
            BCV = bcv_interp.on("SpT", "BCV").evaluate(spts)
        else:
            BCV = bcv_interp("SpT", "BCV").evaluate(spts)
        logl = 0.4*(4.75 + av + 5. * np.log10(dist/10.) - intrinsic_spectrum["V"] - BCV)
        return logl
