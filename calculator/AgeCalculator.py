"""
sippy.calculator.AgeCalculator:
Base python class for inferring any two of age, mass, logT, and logL
given information about the other two parameters.

Uses Bayesian techniques to give confidence intervals for stellar ages.

Author: Lyra Cao
4/12/2018
"""

import numpy as np
from astropy import units as u
import re
from sippy.interpolator.BaseInterpolator import *
import scipy.optimize
from sippy.interpolator.DartmouthInterpolator import \
DartmouthIsochroneInterpolator
from sippy.interpolator.BCAHInterpolator import \
BCAHIsochroneInterpolator
import matplotlib.pyplot as plt
from matplotlib.ticker import NullFormatter
from scipy.spatial import ConvexHull

class priors(object):
    @staticmethod
    def uniform_age_prior(ages):
        max_age = 10.
        prior_array = np.zeros(len(ages))
        cond_not_nan = np.logical_not(np.isnan(prior_array))
        prior_array[cond_not_nan] =\
        np.where(ages[cond_not_nan] > max_age,
                 np.zeros(len(ages[cond_not_nan])),
                 np.zeros(len(ages[cond_not_nan]))+1.)
        return prior_array
    @staticmethod
    def chabrier_mass_prior(masses):
        prior_array = np.zeros(len(masses))
        cond_not_nan = np.logical_not(np.isnan(prior_array))
        prior_array[cond_not_nan] =\
        np.where(masses[cond_not_nan] < 1.,
                 0.093*np.exp(-np.square(np.log10(masses[cond_not_nan])-\
                                         np.log10(0.2))/(2*(0.55**2))),
                 0.041*np.power(masses[cond_not_nan],-1.35))
        return prior_array

class SingleStarAgeMassCalculator(BaseIsochroneInterpolator):
    def __init__(self, IsochroneInterpolator =\
                 BCAHIsochroneInterpolator):
        self.IsochroneInterpolator = IsochroneInterpolator
        self.logage_mass_interp =\
        self.IsochroneInterpolator(("logT", "logL"),
                                   ("logage", "mass"))

        self.logT_logL_interp =\
        self.IsochroneInterpolator(("logage", "mass"),
                                   ("logT", "logL"))
        #Create convex hull of the tracks for HRD plotting.
        self.points_hrd =\
        np.array([[point[2], point[3]] for point in\
                  self.logage_mass_interp.points])
        self.hull_hrd = ConvexHull(points_hrd)
    def _evaluate(self, logT, logL, sigma_logT = None, sigma_logL = None,
                  sigma_limit = 3.,
                  filename = None,
                  num_bins = 101,
                  n_samples = 1000,
                  mass_prior = priors.chabrier_mass_prior,
                  age_prior = priors.uniform_age_prior):
        """Returns a confidence interval if sigma_logT and
        sigma_logL are specified. Otherwise, returns a best value with
        the attached IsochroneInterpolator.
        If `filename` is specified, do a plot and save it to the directory
        requested. If it is not specified, show it to the screen.

        (num_bins should be odd in order to include the best-fit value.)

        n_samples determines the number of samples used to calculate the
        age and mass sigma values."""
        best_logage_mass =\
        self.logage_mass_interp.evaluate(logT, logL)
        if sigma_logT is None or sigma_logL is None:
            #Did not provide sigmas, so just return best ages and masses.
            return best_logage_mass
        ### To find the confidence interval, we need to sample the function at
        ### regular intervals up to a max of sigma_limit * sigma in each
        ### parameter.
        if not hasattr(sigma_logT, "__len__"):
            len_sigma_logT = 0
            if hasattr(logT, "__len__"):
                sigma_logT = np.zeros(len(logT)) + sigma_logT
        if not hasattr(sigma_logL, "__len__"):
            len_sigma_logL = 0
            if hasattr(logL, "__len__"):
                sigma_logL = np.zeros(len(logL)) + sigma_logL
        if not hasattr(logT, "__len__"):
            len_logT = 0
        else:
            len_logT = len(logT)
        if not hasattr(logL, "__len__"):
            len_logL = 0
        else:
            len_logL = len(logL)
        assert len_logT == len_logL and\
        len_logT == len_sigma_logT and\
        len_logL == len_sigma_logL,\
        "Arrays or values passed not of same dimension!"
        ##If I pass in a variety of logT's and logL's, I can get a variety of
        ##logL's and logT's. These define my bounds. Then I regularly sample
        ##in age, mass coordinates.
        logage_est, mass_est =\
        self.logage_mass_interp.\
        evaluate(logT+sigma_logT*np.random.randn(n_samples),
                 logL+sigma_logL*np.random.randn(n_samples))

        sigma_logage = np.nanstd(logage_est)
        sigma_mass = np.nanstd(mass_est)

        # Calculate the range between -sig_lim and +sig_lim
        frac_range = np.linspace(-sigma_limit, sigma_limit, num_bins)
        logage_var = best_logage_mass[0] + frac_range*sigma_logage
        mass_var = best_logage_mass[1] + frac_range*sigma_mass

        logage_grid, mass_grid = np.meshgrid(logage_var, mass_var)
        logT_grid, logL_grid =\
        self.logT_logL_interp.\
        evaluate(logage_grid.flatten(), mass_grid.flatten())

        #grid_points = list(zip(logage_grid.flatten(),
        #                       mass_grid.flatten(),
        #                       logT_grid.flatten(),
        #                       logL_grid.flatten()))
        #print(grid_points)

        chisq =\
        np.square((logT - logT_grid)/sigma_logT) +\
        np.square((logL - logL_grid)/sigma_logL)

        ln_likelihood = -0.5*chisq
        ln_posterior =\
        age_prior(logage_grid.flatten())+\
        mass_prior(mass_grid.flatten())+\
        ln_likelihood ###unnormalized

        # Arbitrarily normalize it by setting the ln peak to 0 to prevent
        # overflow issues.
        ln_posterior -= np.nanmax(ln_posterior)

        #Calculate the posterior:
        posterior = np.exp(ln_posterior)
        #Condition for being finite:
        finite = np.isfinite(posterior)

        #Normalize the posterior:
        logage_width = logage_var[-1] - logage_var[0]
        mass_width = mass_var[-1] - mass_var[0]
        posterior = posterior/(\
        np.nansum(posterior[finite])*(logage_width * mass_width))

        levels = np.linspace(0., np.nanmax(posterior), 50)

        ### Do plotting routines:

        nullfmt = NullFormatter()
        # definitions for the axes
        left, width = 0.1, 0.65
        bottom, height = 0.1, 0.65
        bottom_h = left_h = left + width + 0.02

        rect_bayesian = [left, bottom, width, height]
        rect_histx = [left, bottom_h, width, 0.2]
        rect_histy = [left_h, bottom, 0.2, height]
        rect_hrd = [left_h, bottom_h, 0.2, 0.2]
        rect_colorbar = [left_h + 0.22, bottom, 0.05, 0.87]
        # start with a rectangular Figure
        fig = plt.figure(1, figsize=(12, 12))
        axBayesian = plt.axes(rect_bayesian)
        axHistx = plt.axes(rect_histx)
        axHisty = plt.axes(rect_histy)
        axHistx.xaxis.set_major_formatter(nullfmt)
        axHisty.yaxis.set_major_formatter(nullfmt)
        ax_hrd = plt.axes(rect_hrd)
        ax_hrd.xaxis.set_major_formatter(nullfmt)
        ax_hrd.yaxis.set_major_formatter(nullfmt)

        axBayesian.set_xlim((logage_var[0], logage_var[-1]))
        axBayesian.set_ylim((mass_var[0], mass_var[-1]))
        axHistx.set_xlim(axBayesian.get_xlim())
        axHisty.set_ylim(axBayesian.get_ylim())
        #cb = plt.colorbar(tricontourf_obj, cax = fig.add_axes(rect_colorbar))
        ax_hrd.invert_xaxis()

        for simplex in self.hull_hrd.simplices:
            ax_hrd.plot(self.points_hrd[simplex, 0], self.points_hrd[simplex, 1], 'k-')
        ax_hrd.errorbar(logT,
            logL,
                     xerr = logt_err,
                     yerr = logl_err,
                     ecolor='b',
                     fmt=",",
                     color='b')

        extent = (logage_var[0], logage_var[-1], mass_var[0], mass_var[-1])
        axBayesian.imshow(posterior.reshape(logage_grid.shape), extent = extent,
                          origin="lower", aspect="auto")

        axBayesian.plot(best_logage_mass[0],
        best_logage_mass[1],
        marker="*",
        markersize=10,
        color="white",
        alpha = 1.0,
        markeredgecolor="black",
        markeredgewidth=1.0,
        label = "Peak")
        #plt.contourf(logage_grid, mass_grid,
        #             posterior.reshape(logage_grid.shape), levels = levels)
        #plt.colorbar()

        #logT_var = logT + frac_range*sigma_logT
        #logL_var = logL + frac_range*sigma_logL
        #logT_grid, logL_grid = np.meshgrid(logT_var, logL_var)
        #age_grid, mass_grid =\
        #self.IsochroneInterpolator.evaluate(logT_grid.flatten(),
        #                                    logL_grid.flatten())
        #plt.hist2d(age_grid, mass_grid)

        plt.show()
